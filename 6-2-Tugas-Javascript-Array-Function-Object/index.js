// soal 1
// buatlah variabel seperti di bawah ini

// var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
// urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):

// 1. Tokek
// 2. Komodo
// 3. Cicak
// 4. Ular
// 5. Buaya
console.log('Soal 1');
console.log('');
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
for (var i = 0; i < daftarHewan.sort().length; i++) {
    console.log(daftarHewan[i]);
}


//soal 2
// Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!"

/* 
    Tulis kode function di sini
*/
 
// var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
// var perkenalan = introduce(data)
// console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 
console.log('');
console.log('Soal 2');
console.log('');
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
function Introduce(data) {
    return `Nama saya ${data.name}, umur saya ${data.age} tahun, alamat saya di ${data.address}, dan saya punya hobby yaitu ${data.hobby}`;
}
var perkenalan = Introduce(data);
console.log(perkenalan + "\n"); 


// soal 3
// Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.

// var hitung_1 = hitung_huruf_vokal("Muhammad")

// var hitung_2 = hitung_huruf_vokal("Iqbal")

// console.log(hitung_1 , hitung_2) // 3 2
console.log('');
console.log('Soal 3');
console.log('');
function hitung_huruf_vokal(name) {
    var list_vokal = 'aiueoAIUEO';
    var hitung_vokal = 0;

    for(var i = 0; i < name.length; i++) {
        if(list_vokal.indexOf(name[i]) !== -1) {
            hitung_vokal += 1;
        }
    }
    return hitung_vokal;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");
console.log(hitung_1 , hitung_2, "\n"); // 3 2


// soal 4

// Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.

// console.log( hitung(0) ) // -2
// console.log( hitung(1) ) // 0
// console.log( hitung(2) ) // 2
// console.log( hitung(3) ) // 4
// console.log( hitung(5) ) // 8
console.log('');
console.log('Soal 4');
console.log('');
function hitung(number) {
    var output = -2;

    for (var i = 0; i < number; i++) {
        output += 2;
    }

    return output;
}
console.log( hitung(0) ); // -2
console.log( hitung(1) ); // 0
console.log( hitung(2) ); // 2
console.log( hitung(3) ); // 4
console.log( hitung(5) ); // 8