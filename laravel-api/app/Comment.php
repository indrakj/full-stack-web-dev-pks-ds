<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['content','posts_id','users_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }

            $model->users_id = auth()->user()->id;
        });
    }

    public function post()
    {
        return $this->belongsTo('App\Post', 'posts_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'users_id');
    }
}
