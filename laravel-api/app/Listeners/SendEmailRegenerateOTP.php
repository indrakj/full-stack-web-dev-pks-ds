<?php

namespace App\Listeners;

use App\Mail\RegenerateMail;
use App\Events\RegenerateOTPEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailRegenerateOTP implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegenerateOTPEvent  $event
     * @return void
     */
    public function handle(RegenerateOTPEvent $event)
    {
        // dd($event->otp_code->user->email);
        Mail::to($event->otp_code->user->email)->send(new RegenerateMail($event->otp_code));
    }
}
