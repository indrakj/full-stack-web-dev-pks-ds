<?php
    class Hewan {
        public $nama;
        public $darah;
        public $jumlahKaki;
        public $keahlian;

        public function __construct($nama, $darah, $jumlahKaki, $keahlian, $attackPower, $defencePower)
        {
            $this->nama = $nama;
            $this->darah = $darah;
            $this->jumlahKaki = $jumlahKaki;
            $this->keahlian = $keahlian;
            $this->attackPower = $attackPower;
            $this->defencePower = $defencePower;
        }

        public function atraksi()
        {
            return $this->nama . ' Sedang ' . $this->keahlian;
        }
    }

    class Elang extends Hewan {
        public function getInfoHewan()
        {
            echo 
            'Hewan = '. $this->nama 
            .'<br>'. 
            'Darah = '. $this->darah 
            .'<br>'.  
            'Jumlah Kaki = '. $this->jumlahKaki 
            .'<br>'. 
            'Keahlian = '. $this->keahlian
            .'<br>'.
            'Attack Power = '. $this->attackPower
            .'<br>'.
            'Defence Power = '. $this->defencePower
            .'<br>';
        }
    }
    
    class Harimau extends Hewan {
        public function getInfoHewan()
        {
            echo 
            'Hewan = '. $this->nama 
            .'<br>'. 
            'Darah = '. $this->darah 
            .'<br>'.  
            'Jumlah Kaki = '. $this->jumlahKaki 
            .'<br>'. 
            'Keahlian = '. $this->keahlian
            .'<br>'.
            'Attack Power = '. $this->attackPower
            .'<br>'.
            'Defence Power = '. $this->defencePower
            .'<br>';
        }
    }

    class Fight {
        public $attackPower;
        public $defencePower;

        public function serang($nama1, $nama2)
        {
            echo '<br>';
            return $nama1 . ' sedang menyerang ' . $nama2;
        }

        public function diserang($diserang, $darah, $attPower, $defPower)
        {
            echo '<br>';
            echo $diserang . ' sedang diserang';
            echo '<br>';
            $fight = $darah - $attPower / $defPower;
            echo '<br>';
            return 'Darah ' . $diserang .'=' . $fight;
        }
    }
   
    $elang = new Elang('Eagly', 50, 2, "Terbang Tinggi", 10, 5);
    $harimau = new Harimau('Tigres', 50, 4, "Lari Cepat", 7, 8);
    $fight = new Fight();

    echo '<h1>Atraksi Hewan</h1>';
    echo $elang->atraksi();
    echo '<br>';
    echo $harimau->atraksi();
    echo '<br>';

    echo '<h1>Informasi Hewan</h1>';
    $elang->getInfoHewan();
    echo '<br>';
    $harimau->getInfoHewan();
    echo '<br>';
    
    echo $fight->serang($elang->nama, $harimau->nama);
    echo $fight->diserang($harimau->nama, $harimau->darah, $elang->attackPower, $harimau->defencePower);
    echo '<br>';
    echo $fight->serang($harimau->nama, $elang->nama);
    echo $fight->diserang($elang->nama, $elang->darah, $harimau->attackPower, $elang->defencePower);



?>